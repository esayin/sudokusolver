echo "This shell script will ensure that necesarry libraries and python packages are installed on your system for the sudoku solver to run."
echo "You can find the names of the required libraries under requirements.txt"

if ! command -v pip &> /dev/null; then
    echo "pip is not installed. Please make sure pip is installed and run the script again."
    exit 1
fi

# Check if a requirements.txt file exists
if [ ! -f requirements.txt ]; then
    echo "The requirements.txt file does not exist."
    exit 1
fi

# Install dependencies using pip
echo "Installing dependencies..."
pip install -r requirements.txt

# Check if installation was successful
if [ $? -eq 0 ]; then
    echo "Dependencies installed successfully."
else
    echo "Failed to install dependencies. Please check your setup."
fi

