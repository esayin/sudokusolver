# SudokuSolver

## Aim of this project
This is the gitlab page for the group project SudokuSolver for the P&S course "Python for Engineers 2024"

## Description

This project uses TensorFlow to create a CNN which trains using the MNIST dataset. Before using the CNN to predict the digits from the photo of the sudoku puzzle, the puzzle image gets preprocessed using CV2, PIL, and NumPy. The predicted digits get entered into a 9x9 matrix which is later solved using backtracking. All of the data is then wrapped in a Sudoku GUI using PyGame which allows interaction with the sudoku grid and potential corrections.

## Usage

The requirements are listed under requirements.txt which can all be installed using the install_libs.sh script. This script uses pip to install all dependencies. Make sure you have elevated or sudo permissions.

Before uploading your image, update the path string of the Digit_Pictures file with your own in split_image.py. Also update the IMG_PATH of the sudoku image in the main.py file with your images name. 

There are some sample sudoku games with computer and hand-written digits already in the repository.


## Known Issues

The project was done in limited time which created some issues with our CNN. The image prediction is not always correct. The main problem is with distincting 1's and 7's due to how they are depicted in the MNIST dataset. Again, due to time limitations, the image preprocessing does not account for parallax distortion and angle. Also the image splitting is based on the empty sudoku grid in the repository, we recommend using this grid for any usage.

For any discovered bugs or problems, please create an issue on the gitlab page.
