from solve_sudoku import grid
import celldetection
import split_image
import sudokuGUI

IMG_PATH = "sudoku.jpg"

digit_list = split_image.split(IMG_PATH)

def detect_sudoku(grid, digit_list):
    for i in range(len(digit_list)):
        for j in range(len(digit_list[i])):
            image_path = digit_list[i][j]
            grid[i][j] = celldetection.predict_digit(image_path)


detect_sudoku(grid, digit_list)
sudokuGUI.main()