import pygame as pg
from solve_sudoku import solve, is_valid, grid
import time

#Initialising PyGame
pg.init()

#Defining constants for the GUI

#Size constraints
WIDTH, HEIGHT = 600, 600
SIZE = WIDTH // 9

#Colors
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
GRAY = (200, 200, 200)
BLUE = (0, 0, 255)
RED = (255, 0, 0)

#For shading the selected cell.
HIGHLIGHT_COLOR = (173, 216, 230)


# Set up the display
screen = pg.display.set_mode((WIDTH, HEIGHT))
pg.display.set_caption("Sudoku Solver")



class Grid:
    
    def __init__(self, width = 9,height = 9, status = "incomplete"):
        """
        Main constructor for the grid. Initialises a grid with passed height, width, and status.
        Default values are for a 9x9 sudoku grid which is not yet solved.
        """
        self.width = width
        self.height = height
        self.status = "incomplete"
        self.grid = grid
        self.selected = None
        self.invalid = False

    def draw(self, screen):
        """
        Creating the window with PyGame with the constraints
        """
        #First, Draw the gridlines:
        for i in range(self.width + 1):
            #To distinguish between the 3 cell blocks, we set the line width to 3 if we iterated through 3 cells, else the line width stays 1.
            line_width = 3 if i%3 == 0 else 1
            #Draw the line with the intended width

            #Params of draw.line for self reference: surface, color(tuple), start_pos, end_pos, width of line
            pg.draw.line(screen, BLACK, (i * SIZE, 0), (i * SIZE, HEIGHT), line_width)
            pg.draw.line(screen, BLACK, (0, i * SIZE), (WIDTH, i * SIZE), line_width)

        #Adding highlighting to the cell.
        if self.selected:
            row, col = self.selected
            color = HIGHLIGHT_COLOR if not self.invalid else RED
            pg.draw.rect(screen, color, (col * SIZE, row * SIZE, SIZE, SIZE))

        #Draw the numbers:
        for i in range(self.width):
            for j in range(self.height):
                if self.grid[i][j] != 0:
                    #Will implement seperate function to draw numbers.
                    self.draw_numbers(screen, i, j, self.grid[i][j])
        #Reset validity everytime we update the board
        self.invalid = False

    def draw_numbers(self, screen, row_position, col_position, number):
        """
        Helper function to draw numbers from grid(from solve_sudoku.py) to the board
        """
        #select number font for the numbers:
        
        #Font params source_file (no file so we set it to None), size
        font = pg.font.Font(None, 40)
        #convert the input number to printable string with casting, True for antialiasing(pygame recommends True)
        text = font.render(str(number), True, BLACK)
        #From pygame get_rect(text, style=STYLE_DEFAULT, rotation=0, size=0) -> rect , returns the size and offset of rendered text
        text_rect = text.get_rect(center=(col_position * SIZE + SIZE // 2, row_position * SIZE + SIZE // 2))

        #reference for "blit" == https://www.pygame.org/docs/tut/MoveIt.html?highlight=screen%20blit
        screen.blit(text, text_rect)

        
    def select_cell(self, row_position, col_position):
        """
        Function to call to select a cell in the event of Mousebutton down.
        This event will be handled in the main() loop and called in the event of MB_Down
        """
        self.selected = (row_position,col_position)
        

    def place_number(self, number):
        """
        Function to place a number in the selected cell if it's valid
        """
        
        #This is the old version where we don't highlight the false inputs. 
        #
        #row_position, col_position = self.selected
        #if is_valid(self.grid, row_position, col_position, number):
        #    self.grid[row_position][col_position] = number
        #    return True
        #
        #return False
        if self.selected:
            row, col = self.selected
            self.grid[row][col] = number
            self.invalid = not is_valid(self.grid, row, col, number)
            if not self.invalid:
                self.selected = None



    def clear(self):
        """
        Function to clear a cell. Clear == set to 0 and show blank cell(this is handled by draw_numbers)
        """
        if self.selected:
            row_pos, col_pos = self.selected
            if self.grid[row_pos][col_pos] != 0:
                self.grid[row_pos][col_pos] = 0
                self.invalid = False #resetting validity
    
    def solve_sudoku(self):
        """
        Function to solve sudoku. This already handled in solve_sudoku so we just call the imported function.
        """
        solve(self.grid)





def main():
    """
    MAIN GAME/GUI LOOP
    """
    grid = Grid()
    running = True
    key = None
    #Creating the game loop and handling events (on keypresses/ keydowns)
   
                        
    while running:
        for event in pg.event.get():
            if event.type == pg.QUIT:
                running = False
            elif event.type == pg.MOUSEBUTTONDOWN:
                mouse_position = pg.mouse.get_pos()
                row, col = mouse_position[1] // SIZE, mouse_position[0] // SIZE
                grid.select_cell(row, col)
            elif event.type == pg.KEYDOWN:
                if event.key == pg.K_1: key = 1
                if event.key == pg.K_2: key = 2
                if event.key == pg.K_3: key = 3
                if event.key == pg.K_4: key = 4
                if event.key == pg.K_5: key = 5
                if event.key == pg.K_6: key = 6
                if event.key == pg.K_7: key = 7
                if event.key == pg.K_8: key = 8
                if event.key == pg.K_9: key = 9
                if event.key == pg.K_DELETE:
                    grid.clear()
                    key = None
                if event.key == pg.K_SPACE:
                    grid.solve_sudoku()
                    key = None


        if grid.selected and key is not None:
                grid.place_number(key)
                key = None

        screen.fill(WHITE)
        grid.draw(screen)
        pg.display.flip()
        pg.time.delay(100)

    pg.quit()

if __name__ == "__main__":
    main()


    
