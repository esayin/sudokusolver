import cv2
import numpy as np
import os


#give a path to an image of a sudoku
def split(image_path):
    image = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)

    img_size = image.shape[0]//9
    
    directory = 'Digit_Pictures'


    img = cv2.imread(image_path) 
    if not os.path.isdir(directory):
        os.mkdir(directory) 

    os.chdir(directory)

    paths = [['' for i in range(9)] for j in range(9)]

    for i in range(9):
        for j in range(9):
            img = (image[i*img_size+5:(i+1)*img_size, j*img_size+5:(j+1)*img_size])
            filename = "digit{}.jpg".format(9*i+j)
            paths[i][j] = ("<HERE GOES YOUR PATH\\Digit_Pictures\\>" + filename)
            cv2.imwrite(filename, img) 
    
    print('Successfully saved')
    print(paths)
    #return all images of each digit and empty cell of the sudoku in a 2D list
    return paths

#split('sudoku.jpg')