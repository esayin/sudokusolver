import numpy as np
from tensorflow.keras.datasets import mnist
from tensorflow.keras.models import Sequential, model_from_json
from tensorflow.keras.layers import Dense, Flatten, Conv2D, MaxPooling2D, Dropout
from tensorflow.keras.utils import to_categorical
from PIL import Image, ImageFilter

# Step 1: Load and preprocess the MNIST dataset
(x_train, y_train), (x_test, y_test) = mnist.load_data()
x_train = x_train.reshape(x_train.shape[0], 28, 28, 1).astype('float32')
x_test = x_test.reshape(x_test.shape[0], 28, 28, 1).astype('float32')

x_train = x_train / 255.0
x_test = x_test / 255.0

y_train = to_categorical(y_train, 10)
y_test = to_categorical(y_test, 10)

# Step 2: Create and train the model
model = Sequential([
    Conv2D(32, (5, 5), activation='relu', input_shape=(28, 28, 1)),
    MaxPooling2D(pool_size=(2, 2)),
    Conv2D(16, (3, 3), activation='relu'),
    MaxPooling2D(pool_size=(2, 2)),
    Dropout(0.2),
    Flatten(),
    Dense(128, activation='relu'),
    Dense(64, activation='relu'),
    Dense(10, activation='softmax')
])

model.compile(optimizer='adam',
              loss='categorical_crossentropy',
              metrics=['accuracy'])

model.fit(x_train, y_train, epochs=5, validation_data=(x_test, y_test), batch_size=200)
scores = model.evaluate(x_test, y_test, verbose=0)
print("CNN Error: %.2f%%" % (100 - scores[1] * 100))

# Step 3: Save the model
model_json = model.to_json()
with open("mnist_model.json", "w") as json_file:
    json_file.write(model_json)
model.save_weights("mnist_model.weights.h5")
print("Saved model to disk")

# Step 4: Load the model
json_file = open('mnist_model.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
loaded_model.load_weights("mnist_model.weights.h5")
print("Loaded model from disk")

# Step 5: Preprocess the input image
def preprocess_image(image_path):
    img = Image.open(image_path).convert('L')
    img = img.resize((28, 28), Image.LANCZOS)
    img = img.filter(ImageFilter.GaussianBlur(1))
    img = np.array(img)
    img = 255 - img
    img = img / 255.0
    img = img.reshape(1, 28, 28, 1)
    return img

# Step 6: Make a prediction
def predict_digit(image_path):
    processed_image = preprocess_image(image_path)
    prediction = loaded_model.predict(processed_image)
    predicted_label = np.argmax(prediction)
    return predicted_label

# Example usage
image_path = '/Users/lucaslaser/Downloads/Test07.png'
predicted_digit = predict_digit(image_path)
print(f'Predicted digit: {predicted_digit}')
